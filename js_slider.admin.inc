<?php
/**
 * @file
 *
 * This file is used to handle admin setting for slider.
 *
 */

/**
 * Menu callback: js_slider admin settings .
 */
function js_slider_admin_settings(&$form_state) {

    
  $form = array();

  $form['js_slider_default_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default JS Slider options'),
    '#collapsible' => FALSE,
    '#description' => t(''),
  );
  
  $form['js_slider_default_options']['js_slider_display_field'] = array(
    '#type' => 'select',
    '#title' => t("Display On"),
    '#description' => t("Select page option on which slider will display."),
    '#options' => array(0 => t('Home Page'), 1 => t('All Pages')),
    '#default_value' => variable_get('js_slider_display_field', 0),
  );

  return system_settings_form($form);
}