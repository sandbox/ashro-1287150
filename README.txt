
---------------------------------------------------------------------

                         JS Slider Module

---------------------------------------------------------------------

Description
-----------
This is dynamic JQuery Slider module for image and text contents. 
This slider is based on this blog post. You will find good explanations here.
http://www.smilingsouls.net/Blog/jQuery+Slideshow.html
We modify and integrated this slider into Drupal module, now it is very
easy to manage.
    
 1) Image & Text based contents.
 2) Easy Add/Edit functionality, no need to go in Admin section, direct performs
    operation on slider.
 3) Easy installation, use drupal blocks functionality or use custom code for
    custom theme to show slider.
 4) Easily customizable, if you know CSS you can customize look & feel of slider.


Dependencies for module
-----------------------
 1) Content (CCK module)
 2) FileField
 3) ImageField
 4) Content Copy
 5) Text


Installation steps
------------------
1) Place this module directory in your modules folder (this will usually be
   "sites/all/modules/"). And download the JQuery file from here:-
   http://code.google.com/p/jqueryjs/downloads/detail?name=jquery-1.3.1.min.js
   and place this file into js_files directory. File name should be 
   "jquery-1.3.1.min.js" otherwise it will not run.
2) Enable the module.
   Go to "administer >> build >> modules" and put a checkmark in the 'status'
   column next to 'JS Slider'.
3) Enable permissions appropriate to your site.
4) You can use one of the option to display slider
   a) Use block option
      Go to "administer >> build >> block" and select the region, it should be header,
      or content area because it will not fit into left or right region.
   b) Or use directly this code for custom design/theme <?php print display_slider();?>
5) Initially you will see no slides found message, once you add new slides
   it will start animation.
6) You can manage image size and text from "administer >> content >> content type" and 
   Go "manage field >> Configure" in JS Slider type.

Note:- Please take back-up of your site before installation, its best practice
       to prevent any loss. Do not remove JS Slider content type and its fileds.


Live sites which using this module
----------------------------------
1) http://www.alpl.org/
2) http://www.udrconversion.com/


Author
------
Ocular Concepts

* mail: 
* website:http://www.ocularconcepts.us/

The author can be contacted for paid customizations of this module as well as Drupal development.
