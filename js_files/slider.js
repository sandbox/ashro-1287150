var $$ = $.fn;

$$.extend({
  SplitID : function()
  {
    return this.attr('id').split('-').pop();
  },

  Slideshow : {
    Ready : function()
    {
      $('div.tmpSlideshowControl')
        .hover(
          function() {
            $(this).addClass('tmpSlideshowControlOn');
          },
          function() {
            $(this).removeClass('tmpSlideshowControlOn');
          }
        )
        .click(
          function() {
			$$.Slideshow.Interrupted = false;
			$$.Slideshow.Counter = parseInt(this.id.substring(this.id.length-1));
			$('div.tmpSlide').hide();
			$('div.tmpSlideshowControl').removeClass('tmpSlideshowControlActive');

			$('div#tmpSlide-' + $(this).SplitID()).show()
			$(this).addClass('tmpSlideshowControlActive');
			}
        );

      this.Counter = 1;
      this.Interrupted = false;

      this.Transition();
    },

    Resume : function()
    {
      this.Interrupted = false;  
      this.Transition();      
    },

    Transition : function()
    {
      if (this.Interrupted) {
        return;
      }

      this.Last = this.Counter - 1;

      if (this.Last < 1) {
        this.Last = $('div.tmpSlide').length;
      }

      $('div#tmpSlide-' + this.Last).fadeOut(2000,"linear");
        
      $('div#tmpSlide-' + $$.Slideshow.Counter).fadeIn(
        2000,
        function() {
		  $(this).css('filter','');
		  if(jQuery.browser.msie)
				$(this).css('filter','');
          $('div#tmpSlideshowControl-' + $$.Slideshow.Last).removeClass('tmpSlideshowControlActive');
          $('div#tmpSlideshowControl-' + $$.Slideshow.Counter).addClass('tmpSlideshowControlActive');
          

          $$.Slideshow.Counter++;

          if ($$.Slideshow.Counter > $('div.tmpSlide').length) {
            $$.Slideshow.Counter = 1;
          }

          setTimeout('$$.Slideshow.Transition();', 2000);
        }
      );
    }
  }
});

$(document).ready(
  function() {
    $$.Slideshow.Ready();
  }
);